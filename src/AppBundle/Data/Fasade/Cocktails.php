<?php
/**
 * Created by PhpStorm.
 * User: olici
 * Date: 14.8.16
 * Time: 15:22.
 */

namespace AppBundle\Data\Fasade;

use AppBundle\Entity\Cocktail;
use Doctrine\ORM\EntityManager;

class Cocktails
{
    /**
     * @var EntityManager
     */
    private $em;

    /**
     * Cocktails constructor.
     *
     * @param EntityManager      $em
     */
    public function __construct(EntityManager $em)
    {
        $this->em = $em;
        $this->repository = $this->em->getRepository('AppBundle:Cocktail');
    }

    public function findAll()
    {
        return $this->repository->findAll();
    }

    public function frontedFindAll()
    {
        return $this->repository->createQueryBuilder('e')
            ->getQuery()->getArrayResult();
    }

    /**
     * @param Cocktail $cocktail
     */
    public function save(Cocktail $cocktail)
    {
        $this->em->persist($cocktail);
        $this->em->flush();
    }
}
