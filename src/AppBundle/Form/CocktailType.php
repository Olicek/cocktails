<?php

namespace AppBundle\Form;

use AppBundle\Entity\Cocktail;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\SubmitType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\Form\FormInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;

class CocktailType extends AbstractType
{
    /**
     * @param FormBuilderInterface $builder
     * @param array $options
     */
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add('name')
            ->add('shortDrink')
            ->add('volume')
            ->add('description')
            ->add('active')
            ->add('save', SubmitType::class);
    }

    /**
     * @param OptionsResolver $resolver
     */
    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults([
            'data_class' => 'AppBundle\Entity\Cocktail',
            'label_format' => 'label.%name%',
            'translation_domain' => 'cocktail',
            'empty_data' => function (FormInterface $form) {
                return new Cocktail(
                    $form->getData()['name'],
                    $form->getData()['shortDrink'],
                    $form->getData()['volume'],
                    $form->getData()['active']
                );
            },
        ]);
    }
}
