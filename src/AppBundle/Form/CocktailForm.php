<?php
/**
 * Created by PhpStorm.
 * User: petr
 * Date: 2.10.16
 * Time: 19:41
 */

namespace AppBundle\Form;


use AppBundle\Data\Fasade\Cocktails;
use AppBundle\Entity\Cocktail;
use Symfony\Bundle\FrameworkBundle\Routing\Router;
use Symfony\Bundle\FrameworkBundle\Templating\EngineInterface;
use Symfony\Component\Form\FormFactory;
use Symfony\Component\HttpFoundation\RedirectResponse;
use Symfony\Component\HttpFoundation\Request;
use ZMQContext;


class CocktailForm
{

    /**
     * @var FormFactory
     */
    private $formFactory;

    /**
     * @var Cocktails
     */
    private $cocktails;

    /**
     * @var EngineInterface
     */
    private $template;

    /**
     * @var Router
     */
    private $router;


    /**
     * CocktailForm constructor.
     * @param FormFactory     $formFactory
     * @param Cocktails       $cocktails
     * @param EngineInterface $template
     * @param Router          $router
     */
    public function __construct(
        FormFactory $formFactory,
        Cocktails $cocktails,
        EngineInterface $template,
        Router $router
    ) {
        $this->formFactory = $formFactory;
        $this->cocktails = $cocktails;
        $this->template = $template;
        $this->router = $router;
    }

    public function create(Request $request, Cocktail $cocktail = NULL)
    {
        $form = $this->formFactory->create(CocktailType::class, $cocktail);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            /** @var Cocktail $cocktail */
            $cocktail = $form->getData();
            $id = $cocktail->getId();
            $this->cocktails->save($cocktail);

            $this->pushSocket($cocktail, $id);
            return new RedirectResponse($this->router->generate('cocktailsGrid'));
        }

        return $this->template->renderResponse('AppBundle:cocktail:add.html.twig', [
            'form' => $form->createView(),
        ]);
    }


    /**
     * @param Cocktail $cocktail
     * @param          $id
     */
    private function pushSocket(Cocktail $cocktail, $id)
    {
        if (is_null($id)) {
            $context = new ZMQContext();
            $socket = $context->getSocket(\ZMQ::SOCKET_PUSH, 'my pusher');
            $socket->connect("tcp://localhost:5555");

            $dataToSocket = [
                'entity' => 'cocktail',
                'name' => $cocktail->getName(),
                'id' => $cocktail->getId(),
                'url' => $this->router->generate('editCocktail', ['cocktail' => $cocktail->getId()])
            ];
            $socket->send(json_encode($dataToSocket));
        }
    }

}
