<?php

namespace AppBundle\Entity;

use Doctrine\ORM\Mapping as ORM;
use Gedmo\Mapping\Annotation as Gedmo;
use Gedmo\Translatable\Translatable;
use Symfony\Component\Validator\Constraints as Assert;

/**
 * Cocktail.
 *
 * @ORM\Table(name="cocktail")
 * @ORM\Cache(usage="NONSTRICT_READ_WRITE", region="cocktails")
 * @ORM\Entity(repositoryClass="AppBundle\Repository\CocktailRepository")
 */
class Cocktail implements Translatable
{
    /**
     * @var int
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

    /**
     * @var string
     *
     * @ORM\Column(name="name", type="string", length=160)
     * @Assert\NotBlank()
     * @Gedmo\Translatable()
     */
    private $name;

    /**
     * @var string
     * @Gedmo\Slug(fields={"name"})
     * @ORM\Column(length=160, unique=true)
     * @Gedmo\Translatable()
     */
    private $slug;

    /**
     * @var bool
     *
     * @ORM\Column(name="shortDrink", type="boolean")
     */
    private $shortDrink;

    /**
     * @var float
     *
     * @ORM\Column(name="volume", type="float")
     * @Assert\NotNull()
     */
    private $volume;

    /**
     * @var string
     *
     * @ORM\Column(name="description", type="text", nullable=true)
     * @Gedmo\Translatable()
     */
    private $description;

    /**
     * @var bool
     *
     * @ORM\Column(name="active", type="boolean")
     */
    private $active;

    /**
     * @Gedmo\Locale
     * Used locale to override Translation listener`s locale
     * this is not a mapped field of entity metadata, just a simple property
     * and it is not necessary because globally locale can be set in listener
     */
    private $locale;

    /**
     * Cocktail constructor.
     *
     * @param string $name
     * @param bool   $shortDrink
     * @param float  $volume
     * @param bool   $active
     */
    public function __construct($name, $shortDrink, $volume, $active)
    {
        $this->name = $name;
        $this->shortDrink = $shortDrink;
        $this->volume = $volume;
        $this->active = $active;
    }

    /**
     * Get id.
     *
     * @return int
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set name.
     *
     * @param string $name
     *
     * @return Cocktail
     */
    public function setName($name)
    {
        $this->name = $name;

        return $this;
    }

    /**
     * Get name.
     *
     * @return string
     */
    public function getName()
    {
        return $this->name;
    }

    /**
     * @return string
     */
    public function getSlug()
    {
        return $this->slug;
    }

    /**
     * @param string $slug
     */
    public function setSlug($slug)
    {
        $this->slug = $slug;
    }

    /**
     * Set shortDrink.
     *
     * @param bool $shortDrink
     *
     * @return Cocktail
     */
    public function setShortDrink($shortDrink)
    {
        $this->shortDrink = $shortDrink;

        return $this;
    }

    /**
     * Get shortDrink.
     *
     * @return bool
     */
    public function getShortDrink()
    {
        return $this->shortDrink;
    }

    /**
     * Set volume.
     *
     * @param float $volume
     *
     * @return Cocktail
     */
    public function setVolume($volume)
    {
        $this->volume = $volume;

        return $this;
    }

    /**
     * Get volume.
     *
     * @return float
     */
    public function getVolume()
    {
        return $this->volume;
    }

    /**
     * Set description.
     *
     * @param string $description
     *
     * @return Cocktail
     */
    public function setDescription($description)
    {
        $this->description = $description;

        return $this;
    }

    /**
     * Get description.
     *
     * @return string
     */
    public function getDescription()
    {
        return $this->description;
    }

    /**
     * Set active.
     *
     * @param bool $active
     *
     * @return Cocktail
     */
    public function setActive($active)
    {
        $this->active = $active;

        return $this;
    }

    /**
     * Get active.
     *
     * @return bool
     */
    public function isActive()
    {
        return $this->active;
    }

    public function setTranslatableLocale($locale)
    {
        $this->locale = $locale;
    }
}
