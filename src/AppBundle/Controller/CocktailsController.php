<?php

namespace AppBundle\Controller;

use AppBundle\Data\Fasade\Cocktails;
use AppBundle\Entity\Cocktail;
use AppBundle\Form\CocktailForm;
use CL\Slack\Transport\ApiClient;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Symfony\Bundle\FrameworkBundle\Templating\EngineInterface;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;

class CocktailsController
{
//    use ControllerAwareTrait;

    /**
     * @var Cocktails
     */
    private $cocktails;

    /**
     * @var EngineInterface
     */
    private $template;

    /**
     * @var ApiClient
     */
    private $apiClient;

    /**
     * @var CocktailForm
     */
    private $cocktailForm;


    public function __construct(
        EngineInterface $template,
        Cocktails $cocktails,
        ApiClient $apiClient,
        CocktailForm $cocktailForm
    ) {
        $this->template = $template;
        $this->cocktails = $cocktails;
        $this->apiClient = $apiClient;
        $this->cocktailForm = $cocktailForm;
    }


    /**
     * @Route("/{_locale}/cocktails", name="cocktailsGrid")
     * @return Response
     */
    public function indexAction()
    {
//        $payload = new ChatPostMessagePayload();
//        $payload->setChannel('#general');   // Channel names must begin with a hash-sign '#'
//        $payload->setText('Hello world!');  // also supports Slack formatting
//        $payload->setUsername('acme');      // can be anything you want
//        $payload->setIconEmoji('birthday'); // check out emoji.list-payload for a list of available emojis
//
//        $response = $this->apiClient->send($payload);

        return $this->template->renderResponse('AppBundle:cocktail:index.html.twig', [
            'cocktails' => $this->cocktails->findAll(),
        ]);
    }

    /**
     * @Route("/{_locale}/add-cocktail/{cocktail}-{id}", name="editCocktail", options={
     *     "params"={
     *         "cocktail"={"AppBundle\Entity\Cocktail", "slug"}
     *     }
     * })
     *
     * @param Cocktail $cocktail
     * @param Request $request
     *
     * @return Response
     */
    public function addCocktailAction(Cocktail $cocktail = null, Request $request)
    {
        return $this->cocktailForm->create($request, $cocktail);
    }
}
