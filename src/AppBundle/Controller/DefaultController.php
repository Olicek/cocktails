<?php

namespace AppBundle\Controller;

use AppBundle\ParameterProvider;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Bundle\FrameworkBundle\Templating\EngineInterface;

class DefaultController extends Controller
{
    /**
     * @var EngineInterface
     */
    private $template;
    /**
     * @var ParameterProvider
     */
    private $parameterProvider;

    public function __construct(EngineInterface $template, ParameterProvider $parameterProvider)
    {
        $this->template = $template;
        $this->parameterProvider = $parameterProvider;
    }

    /**
     * @Route("/{_locale}/", name="default")
     */
    public function indexAction()
    {

        return $this->template->renderResponse('default/index.html.twig');
//        return $this->render('default/index.html.twig');
    }
}
