<?php
/**
 * Created by PhpStorm.
 * User: olici
 * Date: 17.9.16
 * Time: 23:17.
 */

namespace AppBundle;

use AppBundle\Data\Fasade\Cocktails;

class ParameterProvider
{
    private $parameters = [];

    public function __construct($rootDir)
    {
        $this->parameters['rootDir'] = $rootDir;
    }

    public function getRootDir()
    {
        return $this->parameters['rootDir'];
    }

    public function setCocktails(Cocktails $cocktails)
    {
        $this->parameters['cocktails'] = $cocktails;
    }

    public function getCocktails()
    {
        return $this->parameters['cocktails'];
    }
}
