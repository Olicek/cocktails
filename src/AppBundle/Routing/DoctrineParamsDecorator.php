<?php

namespace AppBundle\Routing;

use Doctrine\ORM\EntityManager;
use Symfony\Component\Cache\Adapter\AdapterInterface;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Routing\Exception\MethodNotAllowedException;
use Symfony\Component\Routing\Exception\ResourceNotFoundException;
use Symfony\Component\Routing\Matcher\RequestMatcherInterface;
use Symfony\Component\Routing\RequestContext;
use Symfony\Component\Routing\RouterInterface;

final class DoctrineParamsDecorator implements RouterInterface, RequestMatcherInterface
{
    /**
     * @var RouterInterface
     */
    private $router;

    /**
     * @var EntityManager
     */
    private $em;

    /**
     * @var AdapterInterface
     */
    private $adapter;

    public function __construct(RouterInterface $router, EntityManager $em)
    {
        $this->router = $router;
        $this->em = $em;
    }

    public function generate($name, $parameters = [], $referenceType = self::ABSOLUTE_PATH)
    {
        $route = $this->router->getRouteCollection()->get($name);
        $parameters = $this->transformParameters($route, $parameters);

        return $this->router->generate($name, $parameters, $referenceType);
    }

    public function matchRequest(Request $request)
    {
        if (!$this->router instanceof RequestMatcherInterface) {
            throw new \BadMethodCallException('Router has to implement the '.
                RequestMatcherInterface::class);
        }

        return $this->reverseTransformParameters($this->router->matchRequest($request));
    }

    public function getRouteCollection()
    {
        return $this->router->getRouteCollection();
    }

    /**
     * @param \Symfony\Component\Routing\Route $route
     * @param $parameters
     *
     * @return mixed
     *
     * Generate link (URL)
     */
    private function transformParameters($route, $parameters)
    {
        if (null === $route || !$route->hasOption('params')) {
            return $parameters;
        }

        foreach ((array) $route->getOption('params') as $param => list($class, $property)) {
            if (!isset($parameters[$param])) {
                continue;
            }

            $value = $id = $parameters[$param];
            if (is_scalar($value)) {
                $value = $this->em->getRepository($class)->find($value);
            }

            if (!$value instanceof $class) {
                continue;
            }

            $parameters[$param] = $this->em->getClassMetadata($class)->getFieldValue($value, $property);
        }

        return $parameters;
    }

    private function reverseTransformParameters($parameters)
    {
        if (!isset($parameters['_route'])) {
            return $parameters;
        }

        $route = $this->router->getRouteCollection()->get($parameters['_route']);
        if (null === $route || !$route->hasOption('params')) {
            return $parameters;
        }

        foreach ((array) $route->getOption('params') as $param => list($class, $property)) {
            if (!isset($parameters[$param])) {
                continue;
            }

            $value = $parameters[$param];

            if (is_scalar($value)) {
                $value = $this->em->getRepository($class)->findOneBy([$property => $value]);
            }

            if (!$value instanceof $class) {
                continue;
            }

            $parameters[$param] = $value;
        }

        return $parameters;
    }

    /**
     * Sets the request context.
     *
     * @param RequestContext $context The context
     */
    public function setContext(RequestContext $context)
    {
        // TODO: Implement match() method.
    }

    /**
     * Gets the request context.
     *
     * @return RequestContext The context
     */
    public function getContext()
    {
        return $this->router->getContext();
    }

    /**
     * Tries to match a URL path with a set of routes.
     *
     * If the matcher can not find information, it must throw one of the exceptions documented
     * below.
     *
     * @param string $pathinfo The path info to be parsed (raw format, i.e. not urldecoded)
     *
     * @throws ResourceNotFoundException If the resource could not be found
     * @throws MethodNotAllowedException If the resource was found but the request method is not allowed
     * @return array An array of parameters
     *
     */
    public function match($pathinfo)
    {
        // TODO: Implement match() method.
    }
}
